-- load standard vis module, providing parts of the Lua API
require('vis')

-- configure plugins
require('plugins/vis-filetype-settings')

settings = {
	css = {"set tabwidth 2"},
	latex = {
		"map insert ;fb \\\\textbf{}<Escape>hi",
		"map insert ;fi \\\\textit{}<Escape>hi",
		"map insert ;fe \\\\emph{}<Escape>hi",
		"map insert ;fu \\\\underline{}<Escape>hi",
		"map insert ;q \\\\enquote{}<Escape>hi",
		"map insert ;fq \\\\foreignquote{french}{}<Escape>hi",
		"map insert ;v \\\\vec{}<Escape>hi";
		"map insert ;_ _{}<Escape>hi";
		"map insert ;it \\\\begin{itemize}<Enter><Enter>\\\\end{itemize}<Escape>ki\\t\\\\item",
		"map insert ;i \\\\item",
		"map insert ;en \\\\begin{enumerate}<Enter><Enter>\\\\end{enumerate}<Escape>ki\\t\\\\item",
		"map insert ;[ \\\\[\\\\]<Escape>2hi",
	},
}

vis.events.subscribe(vis.events.INIT, function()
	-- Your global configuration options
	vis:command('set theme iceberg')
	vis:command('set syntax on')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	-- Your per window configuration options e.g.
	vis:command('set number')
	vis:command('set autoindent on')
	vis:command('set cursorline on')
	vis:command('set tabwidth 8')
end)
