# Enable vi mode
bindkey -v

# Enable tab completion
autoload -U compinit
compinit
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

# Enable autocorrection
#setopt correctall

# Customize the prompt
autoload -U promptinit
promptinit
prompt gentoo

# Set up command history
export HISTSIZE=10000
export HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh-history"
export SAVEHIST="$HISTSIZE"
# Delete duplicate commands from command history
setopt hist_ignore_all_dups

# Set up eye candy
LS="ls"
if command -v colorls > /dev/null; then LS="colorls -G"; fi
if command -v colortree > /dev/null; then alias tree="colortree -F"; fi

# Add aliases
alias ls="$LS -FHh"
alias ll="ls -l"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias df="df -h"
alias du="du -ch"
alias clbin="curl -F 'clbin=<-' https://clbin.com"
alias fsh=". shortcut-fzf"
alias latexlink="ln -s $XDG_DATA_HOME/texmf/tex/latex/custom-packages/schoolwork.sty ."
alias vpn-up-update="curl ip.me >> $SCRIPTS_DATA_HOME/vpn-up-addresses"
alias ydla="youtube-dl -f 'bestaudio[ext=m4a]'"
