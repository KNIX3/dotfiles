# Gentoo Dotfiles
These dotfiles are managed with GNU Stow. To deploy them, clone them in your home directory, cd into them, and run `stow *`.

## WARNING
I am currently in the process of migrating my dotfiles over to Gentoo. Any OpenBSD specific things will be removed in the near future, or will no longer be supported or maintained.

## Screenshots
![Screenshot 1](https://gitlab.com/KNIX3/dotfiles/-/raw/master/screenshots/1.png)
<details><summary>Additional</summary>
![Screenshot 2](https://gitlab.com/KNIX3/dotfiles/-/raw/master/screenshots/2.png)
![Screenshot 3](https://gitlab.com/KNIX3/dotfiles/-/raw/master/screenshots/3.png)
</details>

## Deployment
As stated above, you can attempt to deploy all dotfiles with `stow *`, but you may not want to do this. You can also deploy individual packages with `stow package`. There are more details in the man page. (`stow(8)` on OpenBSD)

Many of these dotfiles are still not portable to other systems, their home directory hardcoded. You will have to edit these to get them to work.

### A list of dotfiles known to need editing
* git
* gtk (You have to install an Iceberg gtk theme yourself)
* X11/xsession
* drive mounting scripts

I'm working to reduce the amount of non-portable dotfiles, but as of now, I would recommend deploying one package at a time. Additionally, be sure to read any available package specific READMEs before deploying them.
