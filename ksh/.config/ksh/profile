PATH=/bin:/sbin:/usr/bin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/local/jdk-11/bin

# Prioritizes local bin > bin > local scripts
PATH=$HOME/.local/bin:$PATH:$HOME/.local/bin/scripts

# Add ruby gems
PATH=$HOME/.local/share/gem/bin:$PATH

# Add cargo bin
PATH=$PATH:$HOME/.local/share/cargo/bin

export PATH HOME TERM
export ENV="$HOME/.config/ksh/kshrc"

# User locale
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

# Default programs
export EDITOR=nvim
export PAGER=less
export TERMINAL=st
export TERMCMD="$TERMINAL -e"
export READER=zathura
export BROWSER=firefox

# Home clean up via the XDG Base Directory Specification
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export SCRIPTS_DATA_HOME="$XDG_DATA_HOME/scripts"
export LESSHISTFILE="-"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export INPUTRC="$XDG_CONFIG_HOME/shell/inputrc"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export ALSACONFIGDIR="$XDG_CONFIG_HOME/alsa"
export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"
export TERMINFO="$XDG_DATA_HOME/terminfo"
export TERMINFO_DIRS="$XDG_DATA_HOME/terminfo:/usr/share/terminfo"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export M2_REPO="$XDG_DATA_HOME/maven2"
export MAVEN_REPOSITORY="$XDG_DATA_HOME/maven2/repository"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export GOPATH="$XDG_DATA_HOME/go"
export HISTFILE="$XDG_CACHE_HOME/history"
export GRIPHOME="$XDG_CONFIG_HOME/grip"
export MAILRC="$XDG_CONFIG_HOME/mail/mailrc"
export GEM_HOME="$XDG_DATA_HOME/gem"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle

# Specific program settings
export MOZ_WEBRENDER=1
export JAVA_AWT_WM_NONREPARENTING=1
export JAVA_HOME="/usr/local/jdk-11/"
export QT_QPA_PLATFORMTHEME="qt5ct"
export MANPATH="/usr/local/share/man:$HOME/.local/share/man:"
export LESS='-iMRS -x2'
export CLICOLOR=1
export LF_ICONS="$(sed '/^#/d; s/ /=/' $XDG_CONFIG_HOME/lf/icons | tr '\n' ':')"
HISTSIZE=20000
