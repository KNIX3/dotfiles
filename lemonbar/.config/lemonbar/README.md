# lemonbar

Run with `./launch`.

This lemonbar configuration forks four loops into the background that all update different parts of the bar via files stored in $XDG_CACHE_HOME by default. Make sure the cache directory is available to lemonbar. Additionally, you have to manually kill all the loops when you stop lemonbar atm :(
