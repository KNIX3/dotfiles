let b:opened = 'false'
let b:curTex = bufname("%")
let b:curPdf = substitute(bufname("%"), ".tex", ".pdf", "")

function! LatexCompile()
	execute "silent !tectonic " b:curTex
endfunction

function! LatexAndZathura()
	call LatexCompile()
	if b:opened == 'false'
		execute "silent !zathura " b:curPdf "&"
		let b:opened = 'true'
	endif
endfunction

function! LatexReset()
	let b:opened = 'false'
endfunction

nnoremap <A-l> :call LatexAndZathura()<CR>
nnoremap <A-r> :call LatexReset()<CR>
