call plug#begin()
Plug 'arcticicestudio/nord-vim'
Plug 'cocopon/iceberg.vim'
Plug 'itchyny/lightline.vim'
	let g:lightline = {
		\ 'colorscheme': 'iceberg',
		\ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
		\ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" },
	\ }
	set noshowmode
Plug 'ap/vim-css-color'
Plug 'SirVer/ultisnips'
	let g:UltiSnipsExpandTrigger='<tab>'
	let g:UltiSnipsJumpForwardTrigger = '<tab>'
	let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
	let g:UltiSnipsSnippetDirectories=["snips"]
	let g:UltiSnipsSnippetsDir=$HOME.'/.config/neovim/snips'
	let g:tex_flavor = "latex"
call plug#end()

colorscheme iceberg
hi Comment gui=italic cterm=italic term=italic
set termguicolors

let g:loaded_perl_provider = 0
